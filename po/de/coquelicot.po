# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: coquelicot 0.9.6\n"
"PO-Revision-Date: 2016-12-20 12:58+0100\n"
"Last-Translator: potager.org <jardiniers@potager.org>\n"
"Language-Team: potager.org <jardiniers@potager.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. Abbreviated unit of storage. See https://en.wiktionary.org/wiki/tebibyte
#: lib/coquelicot/num.rb:26
msgid "TiB"
msgstr "TiB"

#. Abbreviated unit of storage. See https://en.wiktionary.org/wiki/gibibyte
#: lib/coquelicot/num.rb:28
msgid "GiB"
msgstr "GiB"

#. Abbreviated unit of storage. See https://en.wiktionary.org/wiki/mebibyte
#: lib/coquelicot/num.rb:30
msgid "MiB"
msgstr "MiB"

#. Abbreviated unit of storage. See https://en.wiktionary.org/wiki/kibibyte
#: lib/coquelicot/num.rb:32 views/layout.haml:41
msgid "KiB"
msgstr "KiB"

#. Abbreviated unit of storage. See https://en.wiktionary.org/wiki/byte
#: lib/coquelicot/num.rb:34
msgid "B"
msgstr "B"

#: lib/coquelicot/rack/upload.rb:191
msgid ""
"File is bigger than maximum allowed size: %s would exceed the maximum "
"allowed %s."
msgstr ""
"Die Datei ist größer als die maximal erlaubte Größe: %s würde die\n"
"maximal erlaubten %s überschreiten."

#: lib/coquelicot/rack/upload.rb:194
msgid "File is bigger than maximum allowed size %s."
msgstr "Die Datei ist größer als die maximal erlaubte Größe %s"

#: lib/coquelicot/rack/upload.rb:201
msgid "File has no content"
msgstr "Die Datei hat keinen Inhalt"

#: views/about_your_data.haml:18 views/layout.haml:53
msgid "About your data…"
msgstr "Über deine Daten..."

#: views/about_your_data.haml:20
msgid ""
"Welcome to *Coquelicot*. A simple way to share files with people you know,\n"
"with a little bit of privacy."
msgstr ""
"Willkommen bei *Coquelicot*. Ein einfacher Weg um Dateien mit Freunden zu "
"teilen,\n"
"mit ein bisschen Privatsphäre."

#: views/about_your_data.haml:23
msgid "What should I expect from “a little bit of privacy”?"
msgstr "Was sollte ich von “ein bisschen Privatsphäre” erwarten?"

#: views/about_your_data.haml:25
msgid "Exchanges between your computer and %s are encrypted."
msgstr "Der Austausch zwischen deinem Computer und %s sind verschlüsselt."

#: views/about_your_data.haml:26
msgid ""
"An attacker in-between will be able to see how much data is exchanged,\n"
"but not its nature."
msgstr ""
"Ein zwischengeschalteter Angreifer wird sehen können, wieviel Daten "
"übermittelt werden, aber nicht welcher Art."

#: views/about_your_data.haml:29
msgid ""
"Files are stored encrypted. In case someone gets access to the server\n"
"storage, they will know the size, arrival and expiration dates of the\n"
"files; but they will not be able to get their content without the\n"
"password.\n"
"\n"
"In case no *download password* has been specified, the password might\n"
"be kept in the server request logs. This means that the server might\n"
"store enough information to retrieve the actual file content.\n"
"\n"
"When a *download password* has been specified, the password will not be\n"
"stored anywhere on the server. This will prevent retrieval of the\n"
"file content, except if the server has been actively compromised\n"
"beforehand."
msgstr ""
"Dateien werden verschlüsselt gespeichert. Falls jemand Zugriff auf den "
"Server\n"
"erlangt, werden sie die Größe, Ankunfts- und Ablaufdaten der Files kennen,\n"
" aber sie werden den Inhalt ohne das Passwort nicht sehen können.\n"
"\n"
"Falls kein *Download-Passwort* angegeben wurde, könnte das Passwort in\n"
"den Server-Logs gehalten werden. Das bedeutet, der Server könnte genug\n"
"Informationen vorhalten um an den Datei-Inhalt zu gelangen.\n"
"\n"
"Falls ein *Download-Passwort* angegeben wurde, wird es nirgendwo\n"
"gespeichert werden. Das wird die Abfrage des Datei-Inhalts verhindern,\n"
"es sei denn der Server wurde vorher aktiv kompromitiert."

#: views/about_your_data.haml:43
msgid "What if I don't trust the server admins?"
msgstr "Was is, wenn ich den Server-Admins nicht vertraue?"

#: views/about_your_data.haml:44
msgid ""
"You are [free](http://www.gnu.org/licenses/agpl.txt) to install Coquelicot\n"
"on your own system. Please refer to the [README](README) if you wish to\n"
"know how."
msgstr ""
"Du bist [frei](http://www.gnu.org/licenses/agpl.txt) Coquelicot auf deinem\n"
"Computer zu installieren. Bitte verwende das [README](README), wenn du "
"wissen\n"
"möchtest wie."

#: views/auth/imap.haml:20
msgid "E-mail User:"
msgstr "Email User:"

#: views/auth/imap.haml:23 views/auth/ldap.haml:23 views/enter_file_key.haml:22
msgid "Password:"
msgstr "Passwort:"

#: views/auth/ldap.haml:20
msgid "LDAP User:"
msgstr "LDAP User:"

#: views/auth/simplepass.haml:19 views/auth/userpass.haml:23
msgid "Upload password:"
msgstr "Upload Passwort:"

#: views/auth/userpass.haml:20
msgid "Upload user:"
msgstr "Upload user:"

#: views/download_in_progress.haml:1
msgid "Download in progress"
msgstr "Download läuft"

#: views/download_in_progress.haml:2
msgid "The requested file is currently being downloaded by another client."
msgstr ""
"Die angeforderte Datei wird gerade von einem anderen Client heruntergeladen."

#: views/enter_file_key.haml:18
msgid "Enter download password…"
msgstr "Gib das Download-Passwort ein…"

#: views/error.haml:1
msgid "Error"
msgstr "Fehler:"

#: views/error.haml:2
msgid "Something bad happened: %s"
msgstr "Etwas schlechtes ist passiert: %s"

#: views/expired.haml:18
msgid "Too late…"
msgstr "Zu spät…"

#: views/expired.haml:20
msgid "Sorry, file has expired."
msgstr "Entschuldigung, die Datei ist nicht mehr verfügbar."

#: views/forbidden.haml:1
msgid "Forbidden"
msgstr "Verboten"

#: views/forbidden.haml:2
msgid "This password does not allow access to this resource."
msgstr "Dieses Passwort erlaubt keinen Zugang zu dieser Ressource."

#: views/index.haml:22
msgid "Share a file!"
msgstr "Verteile eine Datei!"

#: views/index.haml:32
msgid "Available for:"
msgstr "Verfügbar für:"

#: views/index.haml:34
msgid "1 day"
msgstr "1 Tag"

#: views/index.haml:34
msgid "1 hour"
msgstr "1 Stunde"

#: views/index.haml:34
msgid "1 month"
msgstr "1 Monat"

#: views/index.haml:34
msgid "1 week"
msgstr "1 Woche"

#: views/index.haml:43
msgid "Unlimited downloads until expiration"
msgstr "Unbeschränkte Downloads bis zum Ablaufdatum"

#: views/index.haml:46
msgid "Remove after one download"
msgstr "Nach einem Download löschen"

#: views/index.haml:48
msgid "Download password (<em>optional</em>):"
msgstr "Download-Passwort (<em>optional</em>):"

#: views/index.haml:51
msgid "File (<em>max. size: %s</em>):"
msgstr "Datei (<em>max. Größe: %s</em>):"

#: views/index.haml:55
msgid "Share!"
msgstr "Verteile!"

#: views/layout.haml:23
msgid "Coquelicot"
msgstr "Coquelicot"

#: views/layout.haml:34
msgid "Generate random"
msgstr "Generiere ein zufälliges"

#: views/layout.haml:35
msgid "Generating…"
msgstr "Generiere…"

#: views/layout.haml:36
msgid "Don't forget to write it down!"
msgstr "Vergiss nicht es aufzuschreiben!"

#: views/layout.haml:37
msgid "Please try again!"
msgstr "Bitte versuch's noch einmal!"

#: views/layout.haml:38
msgid "Error:"
msgstr "Fehler:"

#: views/layout.haml:39
msgid "Upload starting..."
msgstr "Upload startet..."

#: views/layout.haml:40
msgid "Uploading: "
msgstr "Lade hoch: "

#: views/not_found.haml:1
msgid "Not found"
msgstr "Nit gefunden"

#: views/not_found.haml:2
msgid "The requested URL %s was not found on this server."
msgstr "Die angeforderte URL %s wurde nicht auf dem Server gefunden."

#: views/ready.haml:18
msgid "Share this!"
msgstr "Verteile dies!"

#: views/ready.haml:23
msgid "A password is required to download this file."
msgstr "Ein Passwort ist notwendig, um diese Datei herunter zu laden."

#: views/ready.haml:24
msgid "The file will be available until %s."
msgstr "Diese Datei wird verfügbar sein bis %s"

#: views/ready.haml:26
msgid "Share another file…"
msgstr "Verteile eine weitere Datei…"
