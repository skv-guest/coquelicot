# crontab fragment for coquelicot

# Run the garbage collection procedure every 15 minutes
11,26,41,56 * * * * coquelicot [ -x /usr/bin/coquelicot ] && [ -f /etc/coquelicot/settings.yml ] && /usr/bin/coquelicot gc
