#!/bin/sh
# kFreeBSD do not accept scripts as interpreters, using #!/bin/sh and sourcing.
if [ true != "$INIT_D_SCRIPT_SOURCED" ] ; then
    set "$0" "$@"; INIT_D_SCRIPT_SOURCED=true . /lib/init/init-d-script
fi
### BEGIN INIT INFO
# Provides:          coquelicot
# Required-Start:    $remote_fs
# Required-Stop:     $remote_fs
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: "one-click" file sharing web application
# Description:       Coquelicot is a "one-click" file sharing web application
#                    with a focus on protecting users' privacy.
### END INIT INFO

# Author: Jérémy Bobbio <lunar@debian.org>

DESC='Coquelicot "one-click" file sharing web application'
DAEMON=/usr/bin/coquelicot
DAEMON_ARGS="start"
PIDFILE=/var/run/coquelicot/coquelicot.pid

# can be overriden in /etc/default/coquelicot
USER=coquelicot
GROUP=coquelicot

do_start_prepare() {
	START_ARGS="--chuid $USER:$GROUP"
	/usr/bin/install -m 02750 -o "$USER" -g "$USER" -d "$(dirname "$PIDFILE")"
}

# We can't use init-d-script(5) do_stop_cmd() because it matches on the
# executable path and Coquelicot is written in Ruby. So this is almost
# the same function but without `--exec` when calling `start-stop-daemon`.
# We still send QUIT before sending TERM to be hope that worker will
# terminate gracefully.
do_stop_cmd() {
	start-stop-daemon --stop --quiet --retry=QUIT/5/TERM/5/KILL/5 \
	    $STOP_ARGS \
	    ${PIDFILE:+--pidfile ${PIDFILE}} --name $NAME
	RETVAL="$?"
	[ "$RETVAL" = 2 ] && return 2
	# Many daemons don't delete their pidfiles when they exit.
	rm -f $PIDFILE
	return $RETVAL
}
