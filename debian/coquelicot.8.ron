coquelicot(8) -- "one-click" file sharing web application

## SYNOPSIS

`coquelicot` [-c <settings>] start [-n]
`coquelicot` [-c <settings>] stop
`coquelicot` [-c <settings>] gc
`coquelicot` [-c <settings>] migrate-jyraphe [-p <prefix>]<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<jyraphe/var> > <rewrite_rules>

## DESCRIPTION

Coquelicot is a "one-click" file sharing web application with a focus
on protecting users' privacy.

Basic principle: users can upload a file to the server, in return they
get a unique URL which can be shared with others in order to download
the file.

Coquelicot aims to protect, to some extent, users and system
administrators from disclosure of the files exchanged from passive and
not so active attackers.

Coquelicot runs its own web server. It is not recommended to to use it
directly. Coquelicot should rather be made accessible through a non-buffering
HTTPS reverse proxy.

## OPTIONS

  * `-c` </path/to/settings.yml>:
    Specify an alternate configuration file than the default
    `/etc/coquelicot/settings.yml`.

  * `-h`:
    Display usage and exit.

## COMMANDS

 * `start`: start the web server. Unless `-n` is specified, the daemon will be
   put in the background.

 * `stop`: stop the web server.

 * `gc`: run the garbage collection procedure. This is usually run through a
   crontab.

 * `migrate-jyraphe`: run the Jyraphe migration procedure. See README for more
   details.

## FILES

 * `/etc/coquelicot/settings.yml`: main configuration file.

 * `/etc/cron.d/coquelicot`: crontab for scheduling the garbage collection.

 * `/var/lib/coquelicot`: default directory where user files will be stored.

 * `/var/cache/coquelicot`: default directory where cache files will be stored.

 * `/var/log/coquelicot/coquelicot.log`: default log file.

## SEE ALSO

 * `/usr/share/doc/coquelicot/README`,
   `/usr/share/doc/coquelicot/README.Debian`

## AUTHORS

potager.org <jardiniers@potager.org>

mh / immerda.ch  <mh+coquelicot@immerda.ch>

This manual page was written by Jérémy Bobbio <lunar@debian.org>.
